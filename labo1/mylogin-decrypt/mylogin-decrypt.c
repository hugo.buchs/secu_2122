// mylogin-decrypt.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <stdio.h>
#include <stdlib.h>
#include <openssl/evp.h>

int main()
{
	char buffer[20];
	char key[16] = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 };
	FILE* fp;
	errno_t err = 0;
	//Open file
	err = fopen_s(&fp, "mylogin.cnf", "rb");
	if (err == 0 && fp != 0) {
		//Decode key
		err = fseek(fp, 4, SEEK_SET);
		if (err == 0) {
			fread(buffer, sizeof(char), 20, fp);
			for (int i = 0; i < 20; i++) {
				key[i % 16] = (key[i % 16] ^ buffer[i]);
			}

			//Init decryption
			EVP_CIPHER_CTX* ctx;
			ctx = EVP_CIPHER_CTX_new();
			EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);

			//Decrypt each line
			int crypt_length;
			while (fread(&crypt_length, sizeof(int), 1, fp)) {
				char* crypt = malloc(crypt_length);
				if(crypt){
					fread(crypt, sizeof(char), crypt_length, fp);
					int decrypt_length;
					char* decrypt = malloc(crypt_length+EVP_CIPHER_CTX_block_size(ctx));
					if(decrypt){
						int len;
						EVP_DecryptUpdate(ctx, decrypt, &len, crypt, crypt_length);
						decrypt_length = len;
						EVP_DecryptFinal_ex(ctx, decrypt + len, &len);
						decrypt_length += len;
						//Print decrypted line
						printf("%.*s", decrypt_length, decrypt);
						free(decrypt);
					}
					free(crypt);
				}
			}
			EVP_CIPHER_CTX_free(ctx);
		}
		fclose(fp);
	}
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
