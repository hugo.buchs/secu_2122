/*****************
snmp_auth.c
A program to bruteforce SNMPv3 passwords
Hugo Buchs T-3a
******************/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <openssl/blowfish.h>
#include <openssl/evp.h>
#include <string.h>

#define ARRAY_SIZE(x) sizeof(x)/sizeof(x[0]) 

#define STR1_SIZE 1048576
#define MD5_SIZE 16
#define MSG_AUTHID_SIZE 17
#define MSG_AUTHPARAM_SIZE 12
#define EXTENDED_AUTH_SIZE 64
#define FINAL_HASH_LENGTH 24
#define MAX_PASS_LENGTH 100+1

EVP_MD_CTX* ctx;

unsigned char string1[STR1_SIZE];

unsigned char msg_authoritativeengineid[] = {
 0x80, 0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c,
 0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00,
 0x00
};


unsigned char wholemsg[] = {
   0x30, 0x81, 0x81, 0x02, 0x01, 0x03, 0x30, 0x11,
   0x02, 0x04, 0x20, 0xdd, 0x06, 0xa7, 0x02, 0x03,
   0x00, 0xff, 0xe3, 0x04, 0x01, 0x01, 0x02, 0x01,
   0x03, 0x04, 0x31, 0x30, 0x2f, 0x04, 0x11, 0x80,
   0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c, 0x1d,
   0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00, 0x00,
   0x02, 0x01, 0x05, 0x02, 0x01, 0x20, 0x04, 0x04,
   0x75, 0x73, 0x65, 0x72, 0x04, 0x0c, 0x09, 0x44,
   0x5f, 0x87, 0x77, 0x0f, 0x57, 0x52, 0x21, 0x33,
   0x1c, 0x7c, 0x04, 0x00, 0x30, 0x36, 0x04, 0x11,
   0x80, 0x00, 0x1f, 0x88, 0x80, 0xe9, 0xbd, 0x0c,
   0x1d, 0x12, 0x66, 0x7a, 0x51, 0x00, 0x00, 0x00,
   0x00, 0x04, 0x00, 0xa2, 0x1f, 0x02, 0x04, 0x6b,
   0x4c, 0x5a, 0xc2, 0x02, 0x01, 0x00, 0x02, 0x01,
   0x00, 0x30, 0x11, 0x30, 0x0f, 0x06, 0x0a, 0x2b,
   0x06, 0x01, 0x02, 0x01, 0x04, 0x1e, 0x01, 0x05,
   0x02, 0x02, 0x01, 0x01
};


unsigned char msg_authp[] = {
 0x09, 0x44, 0x5f, 0x87, 0x77, 0x0f, 0x57, 0x52,
 0x21, 0x33, 0x1c, 0x7c
};

void gen_snmpv3_hash(unsigned char* password, int password_length, unsigned char* hash, int hash_lenght) {
    unsigned char digest1[MD5_SIZE];
    unsigned char ext_authkey[EXTENDED_AUTH_SIZE];
    unsigned char k1[EXTENDED_AUTH_SIZE];
    unsigned char k2[EXTENDED_AUTH_SIZE];
    unsigned char hash_k1[MD5_SIZE];

    ctx = EVP_MD_CTX_new();
    
    //Create string1
    unsigned int hash_length = 0;
    for (size_t i = 0; i < STR1_SIZE; i++) {
        string1[i] = password[i % password_length];
    }

    EVP_DigestInit_ex(ctx, EVP_md5(), NULL);
    EVP_DigestUpdate(ctx, string1, STR1_SIZE);
    EVP_DigestFinal_ex(ctx, digest1, &hash_length);

    //Create string2
    hash_length = 0;
    int msg_length = MSG_AUTHID_SIZE;
    int digest_length = MD5_SIZE;
    unsigned char string2[MSG_AUTHID_SIZE + MD5_SIZE + MSG_AUTHID_SIZE - 1];

    for (int i = 0; i < digest_length; i++) {
        string2[i] = digest1[i];
    }
    for (int i = 0; i < msg_length; i++) {
        string2[digest_length + i] = msg_authoritativeengineid[i];
    }
    for (int i = 0; i < digest_length; i++) {
        string2[digest_length + msg_length + i] = digest1[i];
    }
    //Hash string2 and fill with zeros to create the extendedAuthKey
    EVP_DigestInit_ex(ctx, EVP_md5(), NULL);
    EVP_DigestUpdate(ctx, string2, ARRAY_SIZE(string2));
    EVP_DigestFinal_ex(ctx, ext_authkey, &hash_length);

    for (int i = MD5_SIZE; i < EXTENDED_AUTH_SIZE; i++) {
        ext_authkey[i] = 0x00;
    }

    //Generate K1
    unsigned char padding[64];
    for (int i = 0; i < EXTENDED_AUTH_SIZE; i++) {
        padding[i] = 0x36;
    }

    for (int i = 0; i < EXTENDED_AUTH_SIZE; i++) {
        k1[i] = ext_authkey[i] ^ padding[i];
    }

    //Generate K2
    for (int i = 0; i < EXTENDED_AUTH_SIZE; i++) {
        padding[i] = 0x5c;
    }

    for (int i = 0; i < EXTENDED_AUTH_SIZE; i++) {
        k2[i] = ext_authkey[i] ^ padding[i];
    }

    //Generate wholeMsgMod using wholeMsg
    unsigned char* wholemsgmod = wholemsg;
    for (int i = 0; i <= ARRAY_SIZE(wholemsgmod); i++) {
        if (wholemsgmod[i] == msg_authp[0] && wholemsgmod[i + 1] == msg_authp[1] && wholemsgmod[i + 3] == msg_authp[3] && wholemsgmod[i + 6] == msg_authp[6]) {
            for (int j = 0; j < MSG_AUTHPARAM_SIZE; j++) {
                wholemsgmod[i + j] = 0x00;
            }
        }
    }

    //Create extK1 by concatenating K1 and wholeMsgMod
    unsigned char ext_k1[ARRAY_SIZE(wholemsgmod) + EXTENDED_AUTH_SIZE];
    hash_length = 0;

    for (int i = 0; i < EXTENDED_AUTH_SIZE; i++) {
        ext_k1[i] = k1[i];
    }
    for (int i = 0; i < ARRAY_SIZE(wholemsgmod); i++) {
        ext_k1[EXTENDED_AUTH_SIZE + i] = wholemsgmod[i];
    }
    //Generate hashK1 from extK1
    EVP_DigestInit_ex(ctx, EVP_md5(), NULL);
    EVP_DigestUpdate(ctx, ext_k1, ARRAY_SIZE(ext_k1));
    EVP_DigestFinal_ex(ctx, hash_k1, &hash_length);

    //Create extK2 by concatenating K2 and hashK1
    unsigned char extK2[MD5_SIZE + EXTENDED_AUTH_SIZE];
    hash_length = 0;

    for (int i = 0; i < EXTENDED_AUTH_SIZE; i++) {
        extK2[i] = k2[i];
    }
    for (int i = 0; i < MD5_SIZE; i++) {
        extK2[EXTENDED_AUTH_SIZE + i] = hash_k1[i];
    }
    //Hash extK2 to get the final hash
    EVP_DigestInit_ex(ctx, EVP_md5(), NULL);
    EVP_DigestUpdate(ctx, extK2, ARRAY_SIZE(extK2));
    EVP_DigestFinal_ex(ctx, hash, &hash_length);

    EVP_MD_CTX_free(ctx);

}

int check(unsigned char* hash, unsigned char* msgauth) {

    for (int i = 0; i < MSG_AUTHPARAM_SIZE; i++) {
        if (msgauth[i] != hash[i]) {
            printf("-> Failure\n\n");
            return 0;
        }
    }

    for (int i = 0; i < MSG_AUTHPARAM_SIZE; i++) {
        printf("%.2x", msgauth[i]);
    }


    for (int i = 0; i < MSG_AUTHPARAM_SIZE; i++) {
        printf("%.2x", hash[i]);
    }
    printf("\nPassword found !\n");
    return 1;
}

int main() {
    FILE* fp;

    unsigned char hash[MD5_SIZE];

    unsigned char* password[MAX_PASS_LENGTH];
    size_t size;

    fp = fopen("./rootmedict.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    int success = 0;

    //Bruteforcing passwords...
    while ((fgets(password, MAX_PASS_LENGTH, fp) != 0) && success == 0) {
        //Print password
        printf("%s", password);
        //Removing end of line char from password
        size = strcspn(password, "\n");
        if (size == 0) continue;
        password[size] = 0;
        //Generate SNMPv3 password hash
        gen_snmpv3_hash(password, size, hash, MD5_SIZE);
        //Check if it matches
        success = check(hash, msg_authp);
    }

    fclose(fp);
    exit(EXIT_SUCCESS);

    return 0;
}
